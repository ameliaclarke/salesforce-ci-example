@IsTest
public with sharing class PetTest {
    @isTest static void testPetCreation_CorrectLanguage() {
        Pet__c p = new Pet__c(
            Name = 'Bob',
            Species__c = 'Cat'
        );

        insert p;

        Pet__c queriedPet = [
            SELECT Id, Language__c
            FROM Pet__c
            WHERE Id = :p.Id
        ];

        System.assertEquals('Meow', queriedPet.Language__c, 'Language should be Meow.');
    }

    @isTest static void testPetCreation_IncorrectLanguage() {
        Pet__c p = new Pet__c(
            Name = 'Bob',
            Species__c = 'Cat'
        );

        insert p;

        Pet__c queriedPet = [
            SELECT Id, Language__c
            FROM Pet__c
            WHERE Id = :p.Id
        ];

        System.assertEquals('Woof', queriedPet.Language__c, 'Language should be Meow.');

    }
}