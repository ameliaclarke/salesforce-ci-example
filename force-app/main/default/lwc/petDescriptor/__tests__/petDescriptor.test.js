import { createElement } from 'lwc';
import PetDescriptor from 'c/petDescriptor';

describe('c-pet-descriptor', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('Shows the correct image', () => {
        // Arrange
        const element = createElement('c-pet-descriptor', {
            is: PetDescriptor
        });

        // Act
        document.body.appendChild(element);

        // Assert
        const div = element.shadowRoot.querySelector('div');
        // expect(1).toBe(1);
        expect(div.textContent).toBe('Cat');
    });

    it('Shows the correct species', () => {
        // Arrange
        const element = createElement('c-pet-descriptor', {
            is: PetDescriptor
        });

        // Act
        document.body.appendChild(element);

        // Assert
        const div = element.shadowRoot.querySelector('div');
        // expect(1).toBe(1);
        expect(div.textContent).toBe('Dog');
    });
});